package com.ingroupe.healthdocconvertor.service;

import com.ingroupe.healthdocconvertor.dto.HdcEntry;
import com.ingroupe.healthdocconvertor.exception.TooManyRequestsException;
import com.ingroupe.healthdocconvertor.repository.GuardRepository;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;


@WebMvcTest(controllers = GuardService.class)
class GuardServiceTest {

    @Autowired
    private GuardService guardService;

    @MockBean
    private GuardRepository guardRepository;

    @Value("${hdc.guard.maxInputConversions}")
    int maxInputConversionsTest;

    @Test
    void givenObjectWithExceededConversions_shouldThrowException() {
        HdcEntry hdcEntry = buildExceededInputConversionsEntry();
        given(this.guardRepository.findByHash("ABC123")).willReturn(hdcEntry);
        assertThrows(TooManyRequestsException.class, () -> guardService.saveHash("ABC123"));
    }

    @NotNull
    private HdcEntry buildExceededInputConversionsEntry() {
        HdcEntry hdcEntry = new HdcEntry();
        hdcEntry.setHash("ABC123");
        for (int i = 0; i < maxInputConversionsTest; i++) {
            hdcEntry.incrementSuccessfulConversions();
        }
        return hdcEntry;
    }

}
