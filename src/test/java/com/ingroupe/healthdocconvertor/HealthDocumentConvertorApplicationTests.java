package com.ingroupe.healthdocconvertor;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Tag("slow")
class HealthDocumentConvertorApplicationTests {

  @Test
  void contextLoads() {
  }

}
