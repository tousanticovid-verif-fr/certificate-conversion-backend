package com.ingroupe.healthdocconvertor.convertor.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ingroupe.healthdocconvertor.connector.deuxddoc.dto.DocumentDto;
import com.ingroupe.healthdocconvertor.connector.deuxddoc.dto.TwoDDocHealthCertResponseFacade;
import com.ingroupe.healthdocconvertor.exception.CannotParse2DDocException;
import com.ingroupe.healthdocconvertor.exception.Invalid2DDocInputValueException;
import ehn.techiop.hcert.kotlin.data.GreenCertificate;
import ehn.techiop.hcert.kotlin.data.RecoveryStatement;
import ehn.techiop.hcert.kotlin.data.Vaccination;
import org.junit.jupiter.api.Test;

import java.time.*;

import static com.ingroupe.healthdocconvertor.util.DateUtils.javaLocalDateOf;
import static com.ingroupe.healthdocconvertor.util.DateUtils.javaZonedDateTimeOf;
import static org.junit.jupiter.api.Assertions.*;

class GreenCertificateRequestAdapterTest {

    private static final String COVID19_DISEASE_AGENT_TARGETED_ID = "840539006";
    private static final String NOT_DETECTED_TEST_RESULT_CODE = "260415000";
    private static final String COVID19_VACCINE_CODE = "J07BX03";

    @Test
    void givenNull_shouldThrowException() {
        CannotParse2DDocException exception =
            assertThrows(CannotParse2DDocException.class, () -> new GreenCertificateRequestAdapter(null));
        assertEquals("The value of the 2DDoc parameter is null", exception.getMessage());
    }

    @Test
    void shouldConvertVaccinationAttestation() throws JsonProcessingException {
        GreenCertificateRequestAdapter adapter = new GreenCertificateRequestAdapter(buildVaccinationTwoDDoc());

        GreenCertificate result = adapter.createEudgcRequest();

        assertEquals("1.3.0", result.getSchemaVersion());
        assertEquals("Bonbeurre", result.getSubject().getFamilyName());
        assertEquals("BONBEURRE", result.getSubject().getFamilyNameTransliterated());
        assertEquals("Jean", result.getSubject().getGivenName());
        assertEquals("JEAN", result.getSubject().getGivenNameTransliterated());
        assertEquals("1962-05-31", result.getDateOfBirthString());

        assert result.getVaccinations() != null;
        Vaccination vaccinationEntry = result.getVaccinations()[0];

        assertEquals(COVID19_DISEASE_AGENT_TARGETED_ID, vaccinationEntry.getTarget().getKey());
        assertEquals(COVID19_VACCINE_CODE, vaccinationEntry.getVaccine().getKey());
        assertEquals("EU/1/20/1528", vaccinationEntry.getMedicinalProduct().getKey());
        assertEquals("ORG-100030215", vaccinationEntry.getAuthorizationHolder().getKey());
        assertEquals(1, vaccinationEntry.getDoseNumber());
        assertEquals(2, vaccinationEntry.getDoseTotalNumber());
        assertEquals(LocalDate.of(2021, 3, 1), javaLocalDateOf(vaccinationEntry.getDate()));
        assertEquals("DGS", vaccinationEntry.getCertificateIssuer());
        assertEquals("", vaccinationEntry.getCertificateIdentifier());
    }

    @Test
    void shouldConvertVaccinationAttestationWithLunarDate() throws JsonProcessingException {
        DocumentDto twoDDoc = buildVaccinationTwoDDoc();
        TwoDDocHealthCertResponseFacade facade = new TwoDDocHealthCertResponseFacade(twoDDoc);
        facade.setVaccineDateOfBirth("01/13/1985");
        GreenCertificateRequestAdapter adapter = new GreenCertificateRequestAdapter(twoDDoc);

        GreenCertificate result = adapter.createEudgcRequest();

        assertEquals("1985", result.getDateOfBirthString());
    }

    private DocumentDto buildVaccinationTwoDDoc() throws JsonProcessingException {
        String json = "{\n" +
            "    \"data\": {\n" +
            "        \"dynamic\": [\n" +
            "            {\n" +
            "                \"duration\": \"3 mois 8 jours\",\n" +
            "                \"vaccination\": \"Cycle vaccinal non valide\",\n" +
            "                \"validityStatus\": \"Non valide\"\n" +
            "            }\n" +
            "        ],\n" +
            "        \"external\": [],\n" +
            "        \"static\": {\n" +
            "            \"annexe\": null,\n" +
            "            \"header\": {\n" +
            "                \"fields\": [\n" +
            "                    {\n" +
            "                        \"label\": \"Type de document\",\n" +
            "                        \"name\": \"00\",\n" +
            "                        \"value\": \"Attestation Vaccinale\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"label\": \"Identifiant de l’autorité de certification\",\n" +
            "                        \"name\": \"03\",\n" +
            "                        \"value\": \"FR00\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"label\": \"Identifiant du certificat\",\n" +
            "                        \"name\": \"04\",\n" +
            "                        \"value\": \"0001\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"label\": \"Date d’émission du document\",\n" +
            "                        \"name\": \"05\",\n" +
            "                        \"value\": \"26/04/2021\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"label\": \"Date de création de la signature\",\n" +
            "                        \"name\": \"06\",\n" +
            "                        \"value\": \"26/04/2021\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"label\": \"Pays émetteur du document\",\n" +
            "                        \"name\": \"09\",\n" +
            "                        \"value\": \"France\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            },\n" +
            "            \"message\": {\n" +
            "                \"fields\": [\n" +
            "                    {\n" +
            "                        \"label\": \"Nom de famille du patient\",\n" +
            "                        \"name\": \"L0\",\n" +
            "                        \"value\": \"Bonbeurre\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"label\": \"Liste des prénoms du patient\",\n" +
            "                        \"name\": \"L1\",\n" +
            "                        \"value\": \"Jean\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"label\": \"Date de naissance du patient\",\n" +
            "                        \"name\": \"L2\",\n" +
            "                        \"value\": \"31/05/1962\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"label\": \"Nom de la maladie couverte\",\n" +
            "                        \"name\": \"L3\",\n" +
            "                        \"value\": \"COVID-19\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"label\": \"Agent prophylactique\",\n" +
            "                        \"name\": \"L4\",\n" +
            "                        \"value\": \"J07BX03\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"label\": \"Nom du vaccin\",\n" +
            "                        \"name\": \"L5\",\n" +
            "                        \"value\": \"PFIZER/BIONTECH – COMIRNATY\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"label\": \"Fabriquant du vaccin\",\n" +
            "                        \"name\": \"L6\",\n" +
            "                        \"value\": \"Pfizer/BioNTech - COMIRNATY\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"label\": \"Rang du dernier état de vaccination effectué\",\n" +
            "                        \"name\": \"L7\",\n" +
            "                        \"value\": \"1\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"label\": \"Nombre de doses attendues pour un cycle complet\",\n" +
            "                        \"name\": \"L8\",\n" +
            "                        \"value\": \"2\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"label\": \"Date du dernier état du cycle de vaccination\",\n" +
            "                        \"name\": \"L9\",\n" +
            "                        \"value\": \"01/03/2021\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"label\": \"Etat du cycle de vaccination\",\n" +
            "                        \"name\": \"LA\",\n" +
            "                        \"value\": \"En cours\"\n" +
            "                    }\n" +
            "                ],\n" +
            "                \"label\": \"Attestation Vaccinale\",\n" +
            "                \"type\": \"L1\"\n" +
            "            },\n" +
            "            \"signature\": {\n" +
            "                \"isValid\": true,\n" +
            "                \"status\": \"Valide\"\n" +
            "            }\n" +
            "        }\n" +
            "    },\n" +
            "    \"errors\": [],\n" +
            "    \"resourceType\": \"2D-DOC_L1\"" +
            "\n" +
            "}";
        return getTwoDDocFromJsonString(json);
    }

    @Test
    void shouldConvertNegativeTestAttestation() throws JsonProcessingException {
        GreenCertificateRequestAdapter adapter = new GreenCertificateRequestAdapter(buildNegativeTestTwoDDoc());

        GreenCertificate result = adapter.createEudgcRequest();

        assertEquals("1.3.0", result.getSchemaVersion());
        assertEquals("Affrite", result.getSubject().getFamilyName());
        assertEquals("AFFRITE", result.getSubject().getFamilyNameTransliterated());
        assertEquals("Barrack", result.getSubject().getGivenName());
        assertEquals("BARRACK", result.getSubject().getGivenNameTransliterated());
        assertEquals("2002-03-04",  result.getDateOfBirthString());

        assert result.getTests() != null;
        ehn.techiop.hcert.kotlin.data.Test testEntry = result.getTests()[0];

        assertEquals(COVID19_DISEASE_AGENT_TARGETED_ID, testEntry.getTarget().getKey());
        assertEquals("LP6464-4", testEntry.getType().getKey());
        assertNull(testEntry.getNameNaa());
        assertNull(testEntry.getNameRat());
        ZonedDateTime expectedSampleDateTime =
            LocalDateTime.of(2021, Month.APRIL, 4, 23, 59)
                .atZone(ZoneId.of("Europe/Paris"))
                .withZoneSameInstant(ZoneOffset.UTC.normalized());
        assertEquals(expectedSampleDateTime,
            javaZonedDateTimeOf(testEntry.getDateTimeSample()));
        assertEquals(NOT_DETECTED_TEST_RESULT_CODE, testEntry.getResultPositive().getKey());
        assertEquals("France", testEntry.getTestFacility());
        assertEquals("FR", testEntry.getCountry());
        assertEquals("DGS", testEntry.getCertificateIssuer());
        assertEquals("", testEntry.getCertificateIdentifier());
    }

    @Test
    void shouldConvertNegativeTestAttestationWithLunarDate() throws JsonProcessingException {
        DocumentDto twoDDoc = buildNegativeTestTwoDDoc();
        TwoDDocHealthCertResponseFacade facade = new TwoDDocHealthCertResponseFacade(twoDDoc);
        facade.setTestDateOfBirth("01/13/1985");
        GreenCertificateRequestAdapter adapter = new GreenCertificateRequestAdapter(twoDDoc);

        GreenCertificate result = adapter.createEudgcRequest();

        assertEquals("1985", result.getDateOfBirthString());
    }

    private DocumentDto buildNegativeTestTwoDDoc() throws JsonProcessingException {
        String json = "{\n" +
            "  \"data\": {\n" +
            "    \"dynamic\": [\n" +
            "      {\n" +
            "        \"duration\": \"65 jours 11 heures 1 minutes\",\n" +
            "        \"result\": \"Test périmé\",\n" +
            "        \"samplingDate\": \"04/04/2021 23:59\",\n" +
            "        \"type\": \"Test non PCR\",\n" +
            "        \"validityStatus\": \"Non valide\"\n" +
            "      }\n" +
            "    ],\n" +
            "    \"external\": [],\n" +
            "    \"static\": {\n" +
            "      \"annexe\": null,\n" +
            "      \"header\": {\n" +
            "        \"fields\": [\n" +
            "          {\n" +
            "            \"label\": \"Type de document\",\n" +
            "            \"name\": \"00\",\n" +
            "            \"value\": \"Test COVID\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Identifiant de l’autorité de certification\",\n" +
            "            \"name\": \"03\",\n" +
            "            \"value\": \"FR03\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Identifiant du certificat\",\n" +
            "            \"name\": \"04\",\n" +
            "            \"value\": \"AHP1\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Date d’émission du document\",\n" +
            "            \"name\": \"05\",\n" +
            "            \"value\": \"05/04/2021\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Date de création de la signature\",\n" +
            "            \"name\": \"06\",\n" +
            "            \"value\": \"12/04/2021\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Pays émetteur du document\",\n" +
            "            \"name\": \"09\",\n" +
            "            \"value\": \"France\"\n" +
            "          }\n" +
            "        ]\n" +
            "      },\n" +
            "      \"message\": {\n" +
            "        \"fields\": [\n" +
            "          {\n" +
            "            \"label\": \"Liste des prénoms\",\n" +
            "            \"name\": \"F0\",\n" +
            "            \"value\": \"Barrack\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Nom de famille\",\n" +
            "            \"name\": \"F1\",\n" +
            "            \"value\": \"Affrite\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Date de naissance\",\n" +
            "            \"name\": \"F2\",\n" +
            "            \"value\": \"04/03/2002\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Genre\",\n" +
            "            \"name\": \"F3\",\n" +
            "            \"value\": \"F\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Code analyse\",\n" +
            "            \"name\": \"F4\",\n" +
            "            \"value\": \"94309-2 PCR COVID\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Résultat de l'analyse\",\n" +
            "            \"name\": \"F5\",\n" +
            "            \"value\": \"Négatif\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Date et heure du prélèvement\",\n" +
            "            \"name\": \"F6\",\n" +
            "            \"value\": \"04/04/2021 23:59\"\n" +
            "          }\n" +
            "        ],\n" +
            "        \"label\": \"Test COVID\",\n" +
            "        \"type\": \"B2\"\n" +
            "      },\n" +
            "      \"signature\": {\n" +
            "        \"isValid\": false,\n" +
            "        \"status\": \"Non valide\"\n" +
            "      }\n" +
            "    }\n" +
            "  },\n" +
            "  \"errors\": [],\n" +
            "  \"resourceType\": \"2D-DOC_B2" +
            "\"\n" +
            "}";
        return getTwoDDocFromJsonString(json);
    }

    @Test
    void shouldConvertPositiveTestAttestation() throws JsonProcessingException {
        GreenCertificateRequestAdapter adapter = new GreenCertificateRequestAdapter(buildPositiveTestTwoDDoc());

        GreenCertificate result = adapter.createEudgcRequest();

        assertEquals("1.3.0", result.getSchemaVersion());
        assertEquals("Couvert", result.getSubject().getFamilyName());
        assertEquals("COUVERT", result.getSubject().getFamilyNameTransliterated());
        assertEquals("Armel", result.getSubject().getGivenName());
        assertEquals("ARMEL", result.getSubject().getGivenNameTransliterated());
        assertEquals("2001-03-02", result.getDateOfBirthString());

        assert result.getRecoveryStatements() != null;
        RecoveryStatement recoveryEntry = result.getRecoveryStatements()[0];

        assertEquals(COVID19_DISEASE_AGENT_TARGETED_ID, recoveryEntry.getTarget().getKey());
        assertEquals(LocalDate.of(2021, 4, 4), javaLocalDateOf(recoveryEntry.getDateOfFirstPositiveTestResult()));
        assertEquals("FR", recoveryEntry.getCountry());
        assertEquals("DGS", recoveryEntry.getCertificateIssuer());
        assertEquals(LocalDate.of(2021, 4, 15), javaLocalDateOf(recoveryEntry.getCertificateValidFrom()));
        assertEquals(LocalDate.of(2021, 10, 1), javaLocalDateOf(recoveryEntry.getCertificateValidUntil()));
        assertEquals("", recoveryEntry.getCertificateIdentifier());
    }

    private DocumentDto buildPositiveTestTwoDDoc() throws JsonProcessingException {
        String json = "{\n" +
            "  \"data\": {\n" +
            "    \"dynamic\": [\n" +
            "      {\n" +
            "        \"duration\": \"65 jours 11 heures 1 minutes\",\n" +
            "        \"result\": \"Test périmé\",\n" +
            "        \"samplingDate\": \"04/04/2021 23:59\",\n" +
            "        \"type\": \"Test non PCR\",\n" +
            "        \"validityStatus\": \"Non valide\"\n" +
            "      }\n" +
            "    ],\n" +
            "    \"external\": [],\n" +
            "    \"static\": {\n" +
            "      \"annexe\": null,\n" +
            "      \"header\": {\n" +
            "        \"fields\": [\n" +
            "          {\n" +
            "            \"label\": \"Type de document\",\n" +
            "            \"name\": \"00\",\n" +
            "            \"value\": \"Test COVID\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Identifiant de l’autorité de certification\",\n" +
            "            \"name\": \"03\",\n" +
            "            \"value\": \"FR03\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Identifiant du certificat\",\n" +
            "            \"name\": \"04\",\n" +
            "            \"value\": \"AHP1\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Date d’émission du document\",\n" +
            "            \"name\": \"05\",\n" +
            "            \"value\": \"05/04/2021\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Date de création de la signature\",\n" +
            "            \"name\": \"06\",\n" +
            "            \"value\": \"12/04/2021\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Pays émetteur du document\",\n" +
            "            \"name\": \"09\",\n" +
            "            \"value\": \"France\"\n" +
            "          }\n" +
            "        ]\n" +
            "      },\n" +
            "      \"message\": {\n" +
            "        \"fields\": [\n" +
            "          {\n" +
            "            \"label\": \"Liste des prénoms\",\n" +
            "            \"name\": \"F0\",\n" +
            "            \"value\": \"Armel\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Nom de famille\",\n" +
            "            \"name\": \"F1\",\n" +
            "            \"value\": \"Couvert\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Date de naissance\",\n" +
            "            \"name\": \"F2\",\n" +
            "            \"value\": \"02/03/2001\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Genre\",\n" +
            "            \"name\": \"F3\",\n" +
            "            \"value\": \"F\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Code analyse\",\n" +
            "            \"name\": \"F4\",\n" +
            "            \"value\": \"94309-2\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Résultat de l'analyse\",\n" +
            "            \"name\": \"F5\",\n" +
            "            \"value\": \"Positif\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Date et heure du prélèvement\",\n" +
            "            \"name\": \"F6\",\n" +
            "            \"value\": \"04/04/2021 23:59\"\n" +
            "          }\n" +
            "        ],\n" +
            "        \"label\": \"Test COVID\",\n" +
            "        \"type\": \"B2\"\n" +
            "      },\n" +
            "      \"signature\": {\n" +
            "        \"isValid\": false,\n" +
            "        \"status\": \"Non valide\"\n" +
            "      }\n" +
            "    }\n" +
            "  },\n" +
            "  \"errors\": [],\n" +
            "  \"resourceType\": \"2D-DOC_B2" +
            "\"\n" +
            "}";
        return getTwoDDocFromJsonString(json);
    }

    @Test
    void givenPositiveAntiBodyTest_shouldThrowException() throws JsonProcessingException {
        GreenCertificateRequestAdapter adapter = new GreenCertificateRequestAdapter(buildPositiveAntibodyTestTwoDDoc());
        assertThrows(Invalid2DDocInputValueException.class, adapter::createEudgcRequest);
    }

    private DocumentDto buildPositiveAntibodyTestTwoDDoc() throws JsonProcessingException {
        String json = "{\n" +
            "  \"data\": {\n" +
            "    \"dynamic\": [\n" +
            "      {\n" +
            "        \"duration\": \"65 jours 11 heures 1 minutes\",\n" +
            "        \"result\": \"Test périmé\",\n" +
            "        \"samplingDate\": \"04/04/2021 23:59\",\n" +
            "        \"type\": \"Test non PCR\",\n" +
            "        \"validityStatus\": \"Non valide\"\n" +
            "      }\n" +
            "    ],\n" +
            "    \"external\": [],\n" +
            "    \"static\": {\n" +
            "      \"annexe\": null,\n" +
            "      \"header\": {\n" +
            "        \"fields\": [\n" +
            "          {\n" +
            "            \"label\": \"Type de document\",\n" +
            "            \"name\": \"00\",\n" +
            "            \"value\": \"Test COVID\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Identifiant de l’autorité de certification\",\n" +
            "            \"name\": \"03\",\n" +
            "            \"value\": \"FR03\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Identifiant du certificat\",\n" +
            "            \"name\": \"04\",\n" +
            "            \"value\": \"AHP1\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Date d’émission du document\",\n" +
            "            \"name\": \"05\",\n" +
            "            \"value\": \"05/04/2021\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Date de création de la signature\",\n" +
            "            \"name\": \"06\",\n" +
            "            \"value\": \"12/04/2021\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Pays émetteur du document\",\n" +
            "            \"name\": \"09\",\n" +
            "            \"value\": \"France\"\n" +
            "          }\n" +
            "        ]\n" +
            "      },\n" +
            "      \"message\": {\n" +
            "        \"fields\": [\n" +
            "          {\n" +
            "            \"label\": \"Liste des prénoms\",\n" +
            "            \"name\": \"F0\",\n" +
            "            \"value\": \"Barrack\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Nom de famille\",\n" +
            "            \"name\": \"F1\",\n" +
            "            \"value\": \"Affrite\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Date de naissance\",\n" +
            "            \"name\": \"F2\",\n" +
            "            \"value\": \"04/03/2002\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Genre\",\n" +
            "            \"name\": \"F3\",\n" +
            "            \"value\": \"F\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Code analyse\",\n" +
            "            \"name\": \"F4\",\n" +
            "            \"value\": \"94558-4 Antigénique COVID\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Résultat de l'analyse\",\n" +
            "            \"name\": \"F5\",\n" +
            "            \"value\": \"Positif\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"label\": \"Date et heure du prélèvement\",\n" +
            "            \"name\": \"F6\",\n" +
            "            \"value\": \"04/04/2021 23:59\"\n" +
            "          }\n" +
            "        ],\n" +
            "        \"label\": \"Test COVID\",\n" +
            "        \"type\": \"B2\"\n" +
            "      },\n" +
            "      \"signature\": {\n" +
            "        \"isValid\": false,\n" +
            "        \"status\": \"Non valide\"\n" +
            "      }\n" +
            "    }\n" +
            "  },\n" +
            "  \"errors\": [],\n" +
            "  \"resourceType\": \"2D-DOC_B2" +
            "\"\n" +
            "}";
        return getTwoDDocFromJsonString(json);
    }

    private DocumentDto getTwoDDocFromJsonString(String json) throws JsonProcessingException {
        return new ObjectMapper().readValue(json, DocumentDto.class);
    }

}
