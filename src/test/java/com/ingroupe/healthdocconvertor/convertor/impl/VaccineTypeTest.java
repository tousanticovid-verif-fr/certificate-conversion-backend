package com.ingroupe.healthdocconvertor.convertor.impl;

import com.ingroupe.healthdocconvertor.exception.Invalid2DDocInputValueException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class VaccineTypeTest {

    @Test
    void givenExactName_shouldReturnExpectedEnum() {
        assertEquals(VaccineType.COMIRNATY, VaccineType.forTwoDDocName("COMIRNATY"));
        assertEquals(VaccineType.COMIRNATY, VaccineType.forTwoDDocName("Comirnaty"));
        assertEquals(VaccineType.MODERNA, VaccineType.forTwoDDocName("MODERNA"));
        assertEquals(VaccineType.ASTRA_ZENECA, VaccineType.forTwoDDocName("ASTRAZENECA"));
        assertEquals(VaccineType.JANSSEN, VaccineType.forTwoDDocName("JANSSEN"));
    }

    @Test
    void givenBiggerName_shouldReturnExpectedEnum() {
        assertEquals(VaccineType.COMIRNATY, VaccineType.forTwoDDocName("PFIZER/BIONTECH – COMIRNATY"));
    }

    @Test
    void givenTwoExactNames_shouldReturnFirstInTheList() {
        assertEquals(VaccineType.COMIRNATY, VaccineType.forTwoDDocName("COMIRNATY MODERNA"));
        assertEquals(VaccineType.COMIRNATY, VaccineType.forTwoDDocName("MODERNA COMIRNATY"));
    }

    @Test
    void givenUnknownVaccineName_shouldThrowException() {
        Invalid2DDocInputValueException exception =
            assertThrows(Invalid2DDocInputValueException.class, () -> VaccineType.forTwoDDocName("ASTRA ZENECA"));
        assertEquals("No corresponding type has been found for the vaccine name", exception.getMessage());
    }


}
