package com.ingroupe.healthdocconvertor.convertor.impl;

import com.ingroupe.healthdocconvertor.exception.Invalid2DDocInputValueException;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VaccinationCycleStateTest {

    @Test
    void givenNull_shouldThrowException() {
        Invalid2DDocInputValueException exception = assertCycleStateThrowsException(null);
        assertEquals("No corresponding type has been found for the vaccine cycle state", exception.getMessage());
    }

    @Test
    void givenEmpty_shouldThrowException() {
        Invalid2DDocInputValueException exception = assertCycleStateThrowsException("");
        assertEquals("No corresponding type has been found for the vaccine cycle state", exception.getMessage());
    }

    @Test
    void givenKnownCycleState_shouldNotThrowException() {
        assertDoesNotThrow(() -> VaccinationCycleState.forTwoDDocState("Terminé"));
    }

    @Test
    void givenUnknownCycleState_shouldThrowException() {
        Invalid2DDocInputValueException exception = assertCycleStateThrowsException("foobar");
        assertEquals("No corresponding type has been found for the vaccine cycle state", exception.getMessage());
    }

    @NotNull
    private Invalid2DDocInputValueException assertCycleStateThrowsException(String cycleStateString) {
        Invalid2DDocInputValueException exception =
            assertThrows(Invalid2DDocInputValueException.class, () -> VaccinationCycleState.forTwoDDocState(cycleStateString));
        assertEquals("No corresponding type has been found for the vaccine cycle state", exception.getMessage());
        return exception;
    }

}
