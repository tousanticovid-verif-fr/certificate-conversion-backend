package com.ingroupe.healthdocconvertor.convertor.impl;

import com.ingroupe.healthdocconvertor.exception.CannotParse2DDocException;
import org.apache.commons.validator.GenericValidator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DateOfBirth {

    private static final Pattern PATTERN = Pattern.compile("(?<day>\\d{2})/(?<month>\\d{2})/(?<year>\\d{4})");
    private final Integer day;
    private final Integer month;
    private final Integer year;
    private final String dateOfBirthString;


    public DateOfBirth( String dateOfBirthString ) {

        if( dateOfBirthString == null || dateOfBirthString.isEmpty() )
            throw new CannotParse2DDocException( buildErrorMessage(dateOfBirthString) );

        this.dateOfBirthString = dateOfBirthString;

        Matcher matcher = PATTERN.matcher(dateOfBirthString);

        if (!matcher.matches())
            throw new CannotParse2DDocException( buildErrorMessage(dateOfBirthString) );

        this.day = Integer.valueOf(matcher.group("day"));
        this.month = Integer.valueOf(matcher.group("month"));
        this.year = Integer.valueOf(matcher.group("year"));

    }

    private String buildErrorMessage(String dateOfBirthString){
        return "The date of birth is invalid : ["+dateOfBirthString+"]";
    }

    // handles shortened dates
    public String format ( ) {

        StringBuilder buf = new StringBuilder(8);

        boolean isDateValid = GenericValidator.isDate(dateOfBirthString, "dd/MM/yyyy", true);

        buf.append(String.format("%04d", year));

        if ( isDateValid ) {
            buf.append( String.format("-%02d", month) );
            buf.append( String.format("-%02d", day) );
        }else if ( month >= 1 && month <=12 ) {
                buf.append( String.format("-%02d", month) );
        }

        return buf.toString();
    }


}
