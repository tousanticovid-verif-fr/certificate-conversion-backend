package com.ingroupe.healthdocconvertor.convertor.impl;

import com.ingroupe.healthdocconvertor.exception.CannotParse2DDocException;

import java.text.Normalizer;

public class NameStandardizer {

    private final String name;

    public NameStandardizer(String name) {
        validateArguments(name);

        this.name = name;
    }

    private void validateArguments(String name) {
        if (name == null || name.isEmpty())
            throw new CannotParse2DDocException("A mandatory variable can not be found in the 2DDoc");
    }

    public String standardize() {
        String transformedName = Normalizer.normalize(name, Normalizer.Form.NFKD).replaceAll(" ", "<").replaceAll("[^[a-zA-Z]<]", "");
        return transformedName.toUpperCase();
    }

}
