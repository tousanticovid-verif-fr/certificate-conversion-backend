package com.ingroupe.healthdocconvertor.convertor.impl;

import com.ingroupe.healthdocconvertor.connector.deuxddoc.TwoDDocDecoderConnector;
import com.ingroupe.healthdocconvertor.connector.deuxddoc.dto.DocumentDto;
import com.ingroupe.healthdocconvertor.connector.dgcissuance.DgcIssuanceConnector;
import com.ingroupe.healthdocconvertor.connector.dgcissuance.EgdcCodeData;
import com.ingroupe.healthdocconvertor.constant.HealthDocumentConvertorConstant;
import com.ingroupe.healthdocconvertor.convertor.HdcStrategy;
import com.ingroupe.healthdocconvertor.exception.CannotParse2DDocException;
import ehn.techiop.hcert.kotlin.data.GreenCertificate;
import kotlinx.serialization.json.Json;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;


@Component
@Slf4j
public class Hdc2dDocToDgcaStrategy implements HdcStrategy {

    @Value("${hdc.connector.deuxDDoc.token}")
    private String token;

    @Autowired
    private TwoDDocDecoderConnector twoDDocDecoderConnector;

    @Autowired
    private DgcIssuanceConnector dgcIssuanceConnector;

    @Override
    public String decodeDocument(String chainEncoded) throws CannotParse2DDocException {
        log.trace("Strategy running {}", Hdc2dDocToDgcaStrategy.class.getSimpleName());

        // token header
        String bearerToken = HealthDocumentConvertorConstant.Web.HEADER_BEARER + StringUtils.SPACE + token;
        log.debug("bearerToken = {}", bearerToken);


        log.debug("Calling 2D-Doc decoder external service. chainEncoded = {}", chainEncoded);
        ResponseEntity<DocumentDto> twoDDocResponse = twoDDocDecoderConnector.decodeTwoDDoc(chainEncoded, bearerToken,
                HealthDocumentConvertorConstant.Web.RESQUEST_PARAM_DECODE_TWO_DDOC);
        log.debug("2D-Doc decoder external service response: twoDDocResponse = {}", twoDDocResponse);

        DocumentDto twoDDoc = twoDDocResponse.getBody();

        new TwoDDocValidator(twoDDoc).validate();
        GreenCertificateRequestAdapter adapter = new GreenCertificateRequestAdapter(twoDDoc);
        GreenCertificate dgcRequest = adapter.createEudgcRequest();

        EgdcCodeData dgc = generateDgc(dgcRequest);

        return dgc.getQrCode();
    }

    private EgdcCodeData generateDgc(GreenCertificate dgcRequest) {
        ResponseEntity<EgdcCodeData> dgc;

        log.debug("Calling Issuance external service. dgcRequest={}", dgcRequest);
        String dgcRequestString = Json.Default.encodeToString(GreenCertificate.Companion.serializer(), dgcRequest);
        dgc = dgcIssuanceConnector.generateDgc(dgcRequestString);
        log.debug("Issuance external service response: dgc={}", dgc);

        return dgc.getBody();
    }


}
