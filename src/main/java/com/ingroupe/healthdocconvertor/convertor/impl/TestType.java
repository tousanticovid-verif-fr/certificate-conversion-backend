package com.ingroupe.healthdocconvertor.convertor.impl;

import com.ingroupe.healthdocconvertor.exception.Invalid2DDocInputValueException;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

public enum TestType {

    PCR(new String[]{"94500-6 PCR COVID", "94309-2 PCR COVID", "94845-5 PCR COVID"}, "LP6464-4"),
    ANTIBODY(new String[]{"94558-4 Antigénique COVID"}, "LP217198-3");

    private final String[] twoDDocType;
    private final String dgcType;

    TestType(String[] twoDDocType, String dgcType) {
        this.twoDDocType = twoDDocType;
        this.dgcType = dgcType;
    }

    public static TestType forTwoDDocType(String twoDDocType) {
        return Arrays.stream(TestType.values())
            .filter(v -> Arrays.stream(v.twoDDocType)
                .anyMatch(s -> StringUtils.equals(twoDDocType, s))
            )
            .findFirst()
            .orElseThrow(() -> new Invalid2DDocInputValueException("No corresponding type has been found for the test type"));
    }

    public String getDgcType() {
        return dgcType;
    }

    public String[] getTwoDDocType() {
        return twoDDocType;
    }
}
