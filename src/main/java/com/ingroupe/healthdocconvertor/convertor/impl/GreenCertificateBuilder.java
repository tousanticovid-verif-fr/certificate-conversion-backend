package com.ingroupe.healthdocconvertor.convertor.impl;

import ehn.techiop.hcert.kotlin.data.*;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.ingroupe.healthdocconvertor.util.DateUtils.kotlinInstantOf;
import static com.ingroupe.healthdocconvertor.util.DateUtils.kotlinLocalDateTimeOf;

public class GreenCertificateBuilder {

    private static final String DISEASE_AGENT_TARGETED_VALUE_SET_ID = "disease-agent-targeted";
    private static final String SCT_VACCINES_COVID_19_VALUE_SET_ID = "sct-vaccines-covid-19";
    private static final String VACCINES_COVID_19_NAMES_VALUE_SET_ID = "vaccines-covid-19-names";
    private static final String VACCINES_COVID_19_AUTH_HOLDERS_VALUE_SET_ID = "vaccines-covid-19-auth-holders";
    private static final String COVID_19_LAB_RESULT_VALUE_SET_ID = "covid-19-lab-result";
    private static final String COVID_19_LAB_TEST_MANUFACTURER_AND_NAME_VALUE_SET_ID = "covid-19-lab-test-manufacturer-and-name";
    public static final String COVID_19_LAB_TEST_TYPE_VALUE_SET_ID = "covid-19-lab-test-type";

    private String schemaVersion;
    private Person subject;
    private String dateOfBirth;
    private List<Vaccination> vaccinations;
    private List<RecoveryStatement> recoveryStatements;
    private List<Test> tests;

    public GreenCertificate build() {
        Vaccination[] vaccinationsArray = null;
        RecoveryStatement[] recoveryStatementsArray = null;
        Test[] testsArray = null;

        if (vaccinations != null && !vaccinations.isEmpty())
            vaccinationsArray = vaccinations.toArray(Vaccination[]::new);

        if (recoveryStatements != null && !recoveryStatements.isEmpty())
            recoveryStatementsArray = recoveryStatements.toArray(RecoveryStatement[]::new);

        if (tests != null && !tests.isEmpty())
            testsArray = tests.toArray(Test[]::new);

        return new GreenCertificate(schemaVersion, subject, dateOfBirth, vaccinationsArray, recoveryStatementsArray, testsArray, null);
    }

    public GreenCertificateBuilder withSchemaVersion(String schemaVersion) {
        this.schemaVersion = schemaVersion;
        return this;
    }

    public PersonBuilder withSubject() {
        return new PersonBuilder();
    }

    public GreenCertificateBuilder withDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
        return this;
    }

    public VaccinationBuilder addVaccination() {
        return new VaccinationBuilder();
    }

    public RecoveryStatementBuilder addRecoveryStatement() {
        return new RecoveryStatementBuilder();
    }

    public TestBuilder addTest() {
        return new TestBuilder();
    }

    public class PersonBuilder {

        private String givenName;
        private String givenNameTransliterated;
        private String familyName;
        private String familyNameTransliterated;

        public PersonBuilder withGivenName(String givenName) {
            this.givenName = givenName;
            return this;
        }

        public PersonBuilder withGivenNameTransliterated(String givenNameTransliterated) {
            this.givenNameTransliterated = givenNameTransliterated;
            return this;
        }

        public PersonBuilder withFamilyName(String familyName) {
            this.familyName = familyName;
            return this;
        }

        public PersonBuilder withFamilyNameTransliterated(String familyNameTransliterated) {
            this.familyNameTransliterated = familyNameTransliterated;
            return this;
        }

        public GreenCertificateBuilder build() {
            GreenCertificateBuilder.this.subject =
                new Person(familyName, familyNameTransliterated, givenName, givenNameTransliterated);
            return GreenCertificateBuilder.this;
        }

    }

    public class VaccinationBuilder {

        private ValueSetEntryAdapter target;
        private ValueSetEntryAdapter vaccine;
        private ValueSetEntryAdapter medicinalProduct;
        private ValueSetEntryAdapter authorizationHolder;
        private int doseNumber;
        private int doseTotalNumber;
        private kotlinx.datetime.LocalDate date;
        private String country;
        private String certificateIssuer;
        private String certificateIdentifier;

        public VaccinationBuilder() {
            certificateIdentifier = "";
        }

        public VaccinationBuilder withTarget(String target) {
            this.target = ValueSetsInstanceHolder.INSTANCE.getINSTANCE().find(DISEASE_AGENT_TARGETED_VALUE_SET_ID, target);
            return this;
        }

        public VaccinationBuilder withVaccine(String vaccine) {
            this.vaccine = ValueSetsInstanceHolder.INSTANCE.getINSTANCE().find(SCT_VACCINES_COVID_19_VALUE_SET_ID, vaccine);
            return this;
        }

        public VaccinationBuilder withMedicinalProduct(String medicinalProduct) {
            this.medicinalProduct = ValueSetsInstanceHolder.INSTANCE.getINSTANCE().find(VACCINES_COVID_19_NAMES_VALUE_SET_ID, medicinalProduct);
            return this;
        }

        public VaccinationBuilder withAuthorizationHolder(String authorizationHolder) {
            this.authorizationHolder = ValueSetsInstanceHolder.INSTANCE.getINSTANCE().find(VACCINES_COVID_19_AUTH_HOLDERS_VALUE_SET_ID, authorizationHolder);
            return this;
        }

        public VaccinationBuilder withDoseNumber(int doseNumber) {
            this.doseNumber = doseNumber;
            return this;
        }

        public VaccinationBuilder withDoseTotalNumber(int doseTotalNumber) {
            this.doseTotalNumber = doseTotalNumber;
            return this;
        }

        public VaccinationBuilder withDate(LocalDate date) {
            this.date = kotlinLocalDateTimeOf(date);
            return this;
        }

        public VaccinationBuilder withCountry(String country) {
            this.country = country;
            return this;
        }

        public VaccinationBuilder withCertificateIssuer(String certificateIssuer) {
            this.certificateIssuer = certificateIssuer;
            return this;
        }

        public VaccinationBuilder withCertificateIdentifier(String certificateIdentifier) {
            this.certificateIdentifier = certificateIdentifier;
            return this;
        }

        public GreenCertificateBuilder build() {
            Vaccination vaccination = new Vaccination(target,
                vaccine,
                medicinalProduct,
                authorizationHolder,
                doseNumber,
                doseTotalNumber,
                date,
                country,
                certificateIssuer,
                certificateIdentifier);

            if (GreenCertificateBuilder.this.vaccinations == null)
                GreenCertificateBuilder.this.vaccinations = new ArrayList<>();

            GreenCertificateBuilder.this.vaccinations.add(vaccination);
            return GreenCertificateBuilder.this;
        }

    }

    public class RecoveryStatementBuilder {

        private ValueSetEntryAdapter target;
        private kotlinx.datetime.LocalDate dateOfFirstPositiveTestResult;
        private String country;
        private String certificateIssuer;
        private kotlinx.datetime.LocalDate certificateValidFrom;
        private kotlinx.datetime.LocalDate certificateValidUntil;
        private String certificateIdentifier;

        public RecoveryStatementBuilder() {
            certificateIdentifier = "";
        }

        public RecoveryStatementBuilder withTarget(String target) {
            this.target = ValueSetsInstanceHolder.INSTANCE.getINSTANCE().find(DISEASE_AGENT_TARGETED_VALUE_SET_ID, target);
            return this;
        }

        public RecoveryStatementBuilder withDateOfFirstPositiveTestResult(LocalDate dateOfFirstPositiveTestResult) {
            this.dateOfFirstPositiveTestResult = kotlinLocalDateTimeOf(dateOfFirstPositiveTestResult);
            return this;
        }

        public RecoveryStatementBuilder withCountry(String country) {
            this.country = country;
            return this;
        }

        public RecoveryStatementBuilder withCertificateIssuer(String certificateIssuer) {
            this.certificateIssuer = certificateIssuer;
            return this;
        }

        public RecoveryStatementBuilder withCertificateValidFrom(LocalDate certificateValidFrom) {
            this.certificateValidFrom = kotlinLocalDateTimeOf(certificateValidFrom);
            return this;
        }

        public RecoveryStatementBuilder withCertificateValidUntil(LocalDate certificateValidUntil) {
            this.certificateValidUntil = kotlinLocalDateTimeOf(certificateValidUntil);
            return this;
        }

        public RecoveryStatementBuilder withCertificateIdentifier(String certificateIdentifier) {
            this.certificateIdentifier = certificateIdentifier;
            return this;
        }

        public GreenCertificateBuilder build() {
            RecoveryStatement recoveryStatement = new RecoveryStatement(target,
                dateOfFirstPositiveTestResult,
                country,
                certificateIssuer,
                certificateValidFrom,
                certificateValidUntil,
                certificateIdentifier);

            if (GreenCertificateBuilder.this.recoveryStatements == null)
                GreenCertificateBuilder.this.recoveryStatements = new ArrayList<>();

            GreenCertificateBuilder.this.recoveryStatements.add(recoveryStatement);
            return GreenCertificateBuilder.this;
        }

    }

    public class TestBuilder {

        private ValueSetEntryAdapter target;
        private ValueSetEntryAdapter type;
        private String nameNaa;
        private ValueSetEntryAdapter nameRat;
        private kotlinx.datetime.Instant dateTimeSample;
        private kotlinx.datetime.Instant dateTimeResult;
        private ValueSetEntryAdapter resultPositive;
        private String testFacility;
        private String country;
        private String certificateIssuer;
        private String certificateIdentifier;

        public TestBuilder() {
            certificateIdentifier = "";
        }

        public TestBuilder withTarget(String target) {
            this.target = ValueSetsInstanceHolder.INSTANCE.getINSTANCE().find(DISEASE_AGENT_TARGETED_VALUE_SET_ID, target);
            return this;
        }

        public TestBuilder withType(String type) {
            this.type = ValueSetsInstanceHolder.INSTANCE.getINSTANCE().find(COVID_19_LAB_TEST_TYPE_VALUE_SET_ID, type);
            return this;
        }

        public TestBuilder withNameNaa(String nameNaa) {
            this.nameNaa = nameNaa;
            return this;
        }

        public TestBuilder withNameRat(String nameRat) {
            this.nameRat = ValueSetsInstanceHolder.INSTANCE.getINSTANCE().find(COVID_19_LAB_TEST_MANUFACTURER_AND_NAME_VALUE_SET_ID, nameRat);
            return this;
        }

        public TestBuilder withDateTimeSample(OffsetDateTime dateTimeSample) {
            this.dateTimeSample = kotlinInstantOf(dateTimeSample);
            return this;
        }

        public TestBuilder withDateTimeResult(OffsetDateTime dateTimeResult) {
            this.dateTimeResult = kotlinInstantOf(dateTimeResult);
            return this;
        }

        public TestBuilder withResultPositive(String resultPositive) {
            this.resultPositive = ValueSetsInstanceHolder.INSTANCE.getINSTANCE().find(COVID_19_LAB_RESULT_VALUE_SET_ID, resultPositive);
            return this;
        }

        public TestBuilder withTestFacility(String testFacility) {
            this.testFacility = testFacility;
            return this;
        }

        public TestBuilder withCountry(String country) {
            this.country = country;
            return this;
        }

        public TestBuilder withCertificateIssuer(String certificateIssuer) {
            this.certificateIssuer = certificateIssuer;
            return this;
        }

        public TestBuilder withCertificateIdentifier(String certificateIdentifier) {
            this.certificateIdentifier = certificateIdentifier;
            return this;
        }

        public GreenCertificateBuilder build() {
            Test test = new Test(target,
                type,
                nameNaa,
                nameRat,
                dateTimeSample,
                dateTimeResult,
                resultPositive,
                testFacility,
                country,
                certificateIssuer,
                certificateIdentifier);

            if (GreenCertificateBuilder.this.tests == null)
                GreenCertificateBuilder.this.tests = new ArrayList<>();

            GreenCertificateBuilder.this.tests.add(test);
            return GreenCertificateBuilder.this;
        }
    }

}
