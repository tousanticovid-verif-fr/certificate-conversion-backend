package com.ingroupe.healthdocconvertor.constant;

public interface HealthDocumentConvertorConstant {

    String CONVERSION_SEPARATOR = "@";

    public interface Web {

        String RESQUEST_PARAM_DECODE_TWO_DDOC = "DCC_CONVERTOR_API";

        String HEADER_BEARER = "Bearer";
    }

}
