package com.ingroupe.healthdocconvertor.dto;

import lombok.Data;

@Data
public class ErrorResponse {

    private String codeError;

}
