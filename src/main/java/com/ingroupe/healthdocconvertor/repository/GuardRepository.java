package com.ingroupe.healthdocconvertor.repository;

import com.ingroupe.healthdocconvertor.dto.HdcEntry;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface GuardRepository extends MongoRepository<HdcEntry, String> {

    HdcEntry findByHash (String hash);

    HdcEntry deleteByHash(String hash);

}
