package com.ingroupe.healthdocconvertor.connector.deuxddoc.dto;

import com.ingroupe.healthdocconvertor.convertor.impl.TestType;
import com.ingroupe.healthdocconvertor.exception.CannotParse2DDocException;
import com.ingroupe.healthdocconvertor.exception.Invalid2DDocInputValueException;
import com.ingroupe.healthdocconvertor.exception.Invalid2DDocPayloadException;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import static com.google.common.collect.MoreCollectors.onlyElement;

public class TwoDDocHealthCertResponseFacade {

    private final DocumentDto twoDDoc;
    private final ZoneDto message;

    public TwoDDocHealthCertResponseFacade(DocumentDto twoDDoc) {
        this.twoDDoc = twoDDoc;
        if (twoDDoc == null)
            throw new Invalid2DDocPayloadException();
        message = twoDDoc.getData().getStaticData().getMessage();
    }

    public AttestationType getAttestationType() {
        if (isTwoDDocIsVaccinationAttestation())
            return AttestationType.VACCINATION;
        else if (isTwoDDocIsTestAttestation()) {
            if (isTestPositive()) {
                if(isAntiBodyTestType()) {
                    return AttestationType.TEST;
                }
                else
                {
                    return AttestationType.RECOVERY;
                }
            } else {
                return AttestationType.TEST;
            }
        }

        throw new CannotParse2DDocException(twoDDoc, "The type of 2DDoc is not supported");
    }

    public boolean isAntiBodyTestType() {
        return Arrays.stream(TestType.ANTIBODY.getTwoDDocType()).anyMatch(m -> m.equals(getTestType()));
    }

    private boolean isTwoDDocIsVaccinationAttestation() {
        return message.getType().equals("L1");
    }

    private boolean isTwoDDocIsTestAttestation() {
        return message.getType().equals("B2");
    }

    public boolean isTestPositive() {
        String value = getTestAnalysisResult();

        switch (value) {
            case "Positif":
                return true;
            case "Négatif":
                return false;
            default:
                throw new Invalid2DDocInputValueException( "Test result is not supported" );
        }
    }

    public String getVaccinePatientFirstName() {
        return getValueFromFields(message.getFields(), "L1");
    }

    public String getVaccinePatientLastName() {
        return getValueFromFields(message.getFields(), "L0");
    }

    public String getVaccineDateOfBirth() {
        return getValueFromFields(message.getFields(), "L2");
    }

    public void setVaccineDateOfBirth(String dateOfBirth) {
        setValueToField(message.getFields(), "L2", dateOfBirth);
    }

    public String getVaccineName() {
        return getValueFromFields(message.getFields(), "L5");
    }

    public String getVaccineManufacturerName() {
        return getValueFromFields(message.getFields(), "L6");
    }

    public String getVaccineDoseNumber() {
        return getValueFromFields(message.getFields(), "L7");
    }

    public String getVaccineTotalDoses() {
        return getValueFromFields(message.getFields(), "L8");
    }

    public String getVaccinationDate() {
        return getValueFromFields(message.getFields(), "L9");
    }

    public String getVaccinationCycleState() {
        return getValueFromFields(message.getFields(), "LA");
    }

    public String getTestPatientFirstName() {
        return getValueFromFields(message.getFields(), "F0");
    }

    public String getTestPatientLastName() {
        return getValueFromFields(message.getFields(), "F1");
    }

    public String getTestDateOfBirth() {
        return getValueFromFields(message.getFields(), "F2");
    }

    public void setTestDateOfBirth(String dateOfBirth) {
        setValueToField(message.getFields(), "F2", dateOfBirth);
    }

    public String getTestType() {
        return getValueFromFields(message.getFields(), "F4");
    }

    public String getTestAnalysisResult() {
        return getValueFromFields(message.getFields(), "F5");
    }

    public String getTestDate() {
        return getValueFromFields(message.getFields(), "F6");
    }

    private String getValueFromFields(List<FieldDto> fields, String name) {
        try {
            return getField(fields, name).getValue();
        } catch (NoSuchElementException e) {
            throw new CannotParse2DDocException("A mandatory variable can not be found in the 2DDoc");
        }
    }

    private FieldDto getField(List<FieldDto> fields, String name) {
        try {
            return fields.stream()
                    .filter(f -> f.getName().equals(name))
                    .collect(onlyElement());
        } catch (IllegalArgumentException e) {
            throw new CannotParse2DDocException("The same variable has been found more than once in the 2DDoc");
        }
    }

    private void setValueToField(List<FieldDto> fields, String name, String value) {
        FieldDto field = getFieldOrAddNew(fields, name);
        field.setValue(value);
    }

    private FieldDto getFieldOrAddNew(List<FieldDto> fields, String name) {
        try {
            return getField(fields, name);
        } catch (NoSuchElementException e) {
            FieldDto field = new FieldDto();
            field.setName(name);
            fields.add(field);

            return field;
        }
    }

    public boolean isSignatureValid() {
        return twoDDoc.getData().getStaticData().getSignature().isValid();
    }

    public boolean isSignatureBlackListed() {
        return twoDDoc.getData().getStaticData().getIsBlacklisted();
    }

}
