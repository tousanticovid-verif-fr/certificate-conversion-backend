package com.ingroupe.healthdocconvertor.connector.deuxddoc.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class DocumentDto {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String resourceType;

    private DataDto data;

    private List<ErrorDto> errors;

}
