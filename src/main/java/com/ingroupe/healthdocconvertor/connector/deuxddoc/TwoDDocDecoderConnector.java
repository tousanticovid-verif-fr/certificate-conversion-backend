package com.ingroupe.healthdocconvertor.connector.deuxddoc;

import com.ingroupe.healthdocconvertor.connector.deuxddoc.dto.DocumentDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

@Component
@FeignClient(name = "hdc-connector-deuxDDoc", url = "${hdc.connector.deuxDDoc.baseUrl}", configuration = {
        TwoDDocDecoderConnectorConfig.class })
public interface TwoDDocDecoderConnector {

    @GetMapping(value = { "${hdc.connector.deuxDDoc.path}" }, produces = { "application/json" })
    ResponseEntity<DocumentDto> decodeTwoDDoc(@RequestParam(name = "2ddoc") String chainEncoded,
            @RequestHeader("Authorization") String bearerToken,
            @RequestParam("clientControlType") String clientControlType);

}
