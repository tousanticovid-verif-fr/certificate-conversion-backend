package com.ingroupe.healthdocconvertor.connector.deuxddoc.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
import java.util.Map;

@Getter
@Setter
@ToString
public class DataDto {
    @JsonProperty("dynamic")
    private List<Map<String, Object>> dynamicData;

    @JsonProperty("static")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Barcode2DDocDto staticData;

    @JsonProperty("external")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<Object> externalData;
}
