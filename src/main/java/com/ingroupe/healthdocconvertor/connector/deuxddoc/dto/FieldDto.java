package com.ingroupe.healthdocconvertor.connector.deuxddoc.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class FieldDto {
    private String name;

    private String label;

    private String value;
}
