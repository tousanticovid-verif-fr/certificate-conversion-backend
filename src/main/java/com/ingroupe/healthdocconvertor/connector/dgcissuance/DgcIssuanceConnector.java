package com.ingroupe.healthdocconvertor.connector.dgcissuance;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Component
@FeignClient(name = "hdc-connector-dgcIssuance", url = "${hdc.connector.dgcIssuance.baseUrl}")
public interface DgcIssuanceConnector {

    @PutMapping(value = { "${hdc.connector.dgcIssuance.path}" }, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<EgdcCodeData> generateDgc(@RequestBody String dgc);

}
