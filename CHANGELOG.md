# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## Unreleased

## 1.2.8 - 2021-12-22
### Removed
- 2DDOC input log

### Fixed
- 2DDOC fraud extraction with URL encoding

## 1.2.7 - 2021-12-10
### Added
- 2DDOC input log

## 1.2.6 - 2021-11-23
### Added
- Fraud detection : guard API being disabled, mongoDB hash retention (daily purge)
- add stacktrace logging if unknown error

## 1.2.5 - 2021-10-22
### Changed
- Error codes revision : blacklists and external API return codes

## 1.2.4 - 2021-10-15
### Changed
- Error codes revision


## 1.2.3 - 2021-09-24
### Removed
- Better error handling when the vaccine cycle state is unknown: a new `UNKNOWN_VACCINE_STATE_CYCLE` error code is now returned

### Fixed
- Blacklist exception


## 1.2.2 - 2021-09-22
### Fix
- hcert-kotlin dependency version

## 1.2.1 - 2021-09-22
### Removed
- Remove DCC activity feature

### Changed
- update swagger
- blacklist check : now from TACV API

## 1.2.0 - 2021-09-17
### Added
- Better error handling when the vaccine cycle state is unknown: a new `UNKNOWN_VACCINE_STATE_CYCLE` error code is now returned
- DCC activity

### Changed
- To comply with GDPR regulation, the original 2D-Doc has been hidden from the logs when an error occurs on another internal micro-service call
- The details in the error message have been hidden


## 1.1.1 - 2021-08-18

### Changed

- Improved support of lunar dates of birth
- change Nexus endpoint to HTTPS
- change docs directory


## 1.1.0 - 2021-08-13

### Changed

- Made 2D-Doc signature blacklist case-insensitive

### Added

- Added support of lunar dates of birth


## 1.0.0 - 2021-08-09

### Changed

Update `hcert-kotlin-jvm` to v1.2.0 containing DCC JSON schema v1.3.0


## 0.0.7 - 2021-08-07

### Added

Add 2D-Doc blacklist based on their signature


## 0.0.6.1 - 2021-07-09

### Changed

- Add vaccination type condition for the error on different number of doses

## 0.0.6 - 2021-07-08

### Changed

- Return error when vaccination cycle is completed but number of doses is different


## 0.0.5 - 2021-06-30

### Changed

- Update `hcert-kotlin-jvm` version to `1.0.1` to make the app compatible with DCC JSON schema version `1.2.1`
- The 2D-Doc vaccine name matching rule is now more tolerant: there is a match when the given name contains a matching word (e.g., if the given 2D-Doc vaccine name is `Pfizer/Biontech – Comirnaty` or `Comirnaty`, then the matching vaccine is _Pfizer_ vaccine)

### Fixed

- `v` (vaccine), `t` (test), and `r` (recovery) arrays in the response are now **not** initialized as it could cause the resulting DCC to not be readable.


## 0.0.4 - 2021-06-29 [YANKED]

(This release has been rolled back)


## 0.0.3.1 - 2021-06-24

### Fixed

- Add dash to vaccine manufacturer code names (e.g., `ORG-100030215` instead of `ORG100030215`)


## 0.0.3 - 2021-06-24

### Changed

- A positive antigen test in a 2D-Doc is considered as a test in DCC
- A positive PCR test in a 2D-Doc is considered as a recovery in DCC


## 0.0.2 - 2021-06-16

### Added

- Add 2D-Doc decoder token


## 0.0.1 - 2021-06-15

Initial release
