![IN Groupe](https://res.cloudinary.com/ingroupe/image/upload/v1576850289/ingroupe-logo-mini.png)

Le code présenté ci-dessous est celui de l’API de conversion permettant de convertir une preuve sanitaire en format 2D-Doc en QR européen (DCC).

Ce code est publié sous une licence propre à IN Groupe disponible ici dans le fichier [`LICENSE`](LICENSE), que toute personne s’engage à consulter et respecter avant toute utilisation du code objet de la présente publication.


# Usage et fonctionnalité

La conversion d’un 2D-Doc au format européen est effectuée selon la séquence suivante :

1. La demande de conversion au format européen est effectuée depuis l’application Tous AntiCovid
2. Le 2D-DOC est transmis au serveur IN Groupe en bénéficiant d’un chiffrement de bout en bout pour protéger les données personnelles contenues dans le 2D-Doc.
3. Une fois reçu sur le site d’IN Groupe, l’API de conversion réalise la conversion de 2D-Doc en QR code européen selon la séquence suivante :
   1. vérification de la validité et de l’authenticité du 2D-Doc reçu et extraction des données
   2. Conversion des données nécessaires à la génération du QR européen sur la base des informations extraites du 2D-Doc
   3. Génération et signature du QR européen
4. Le QR code européen est ensuite renvoyé à l’application Tous Anti Covid en bénéficiant d’un chiffrement de bout en bout pour protéger les données personnelles contenues dans le QR Code européen.


# Modalité de publication

Le code publié contient l’ensemble des règles de gestion permettant de convertir les données nécessaires à la génération du QR européen effectué dans les serveurs d’IN Groupe.

La vérification de la validité du 2D-Doc reçu est effectuée par un appel à une API interne de vérification qui est caviardée pour des raisons de sécurité.

La génération du QR européen est effectuée par un appel à une API interne de génération basée sur le code source mis à disposition par l’UE et disponible à l’adresse suivante : https://github.com/eu-digital-green-certificates/dgca-issuance-service


# Licences

## Apache 2.0

### Component: Electronic Health Certificate Kotlin Multiplatform Library

License Text URL: [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Source Code: [https://github.com/ehn-dcc-development/hcert-kotlin](https://github.com/ehn-dcc-development/hcert-kotlin)

### Component: Spring Boot

License Text URL: [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Source Code: [https://github.com/spring-projects/spring-boot](https://github.com/spring-projects/spring-boot)

### Component: Spring Cloud OpenFeign

License Text URL: [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Source Code: [https://github.com/spring-cloud/spring-cloud-openfeign](https://github.com/spring-cloud/spring-cloud-openfeign)

### Component: Spring Cloud Consul

License Text URL: [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Source Code: [https://github.com/spring-cloud/spring-cloud-consul](https://github.com/spring-cloud/spring-cloud-consul)

### Component: Guava

License Text URL: [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Source Code: [https://github.com/google/guava](https://github.com/google/guava)

### Component: kotlinx-datetime

License Text URL: [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Source Code: [https://github.com/Kotlin/kotlinx-datetime](https://github.com/Kotlin/kotlinx-datetime)

### Component: kotlinx.serialization

License Text URL: [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Source Code: [https://github.com/Kotlin/kotlinx.serialization](https://github.com/Kotlin/kotlinx.serialization)

### Component: hcert-kotlin

License Text URL: [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Source Code: [https://github.com/ehn-dcc-development/hcert-kotlin](https://github.com/ehn-dcc-development/hcert-kotlin)

### Component: Springdoc OpenAPI

License Text URL: [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Source Code: [https://github.com/springdoc/springdoc-openapi](https://github.com/springdoc/springdoc-openapi)

### Component: Feign

License Text URL: [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Source Code: [https://github.com/OpenFeign/feign](https://github.com/OpenFeign/feign)


## MIT

### Component: Project Lombok

License Text URL: [https://github.com/Kitura/Swift-JWT/blob/master/LICENSE](https://github.com/Kitura/Swift-JWT/blob/master/LICENSE)

Source Code: [https://github.com/projectlombok/lombok](https://github.com/projectlombok/lombok)
